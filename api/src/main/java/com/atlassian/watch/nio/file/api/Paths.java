package com.atlassian.watch.nio.file.api;

import com.atlassian.watch.nio.file.internal.NativeCapable;
import com.atlassian.watch.nio.file.internal.java7.PathImpl;

import java.io.File;
import java.io.IOException;

/**
 * See http://docs.oracle.com/javase/7/docs/api/java/nio/file/Paths.html
 */
public class Paths
{

    /**
     * Converts a file path string into a Path object
     *
     * @param fullPath the file path to convert
     * @return the newly created Path object
     */
    public static Path get(String fullPath)
    {
        return toPath(fullPath);
    }

    /**
     * Converts a file path string into a Path object
     *
     * @param fullPath the file path to convert
     * @return the newly created Path object
     */
    public static Path toPath(String fullPath)
    {
        if (NativeCapable.isCapable())
        {
            return PathImpl.toPath(fullPath);
        }
        else
        {
            return com.atlassian.watch.nio.file.internal.jwatch.PathImpl.toPath(fullPath);
        }
    }

    /**
     * Converts a file into a Path object
     *
     * @param file the file to convert
     * @return the newly created Path object
     */
    public static Path toPath(File file)
    {
        try
        {
            return toPath(file.getCanonicalPath());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
