package com.atlassian.watch.nio.file.internal.jwatch;

import com.atlassian.watch.nio.file.api.Path;
import com.atlassian.watch.nio.file.api.WatchEvent;
import com.atlassian.watch.nio.file.api.WatchKey;
import com.atlassian.watch.nio.file.api.WatchService;
import com.atlassian.watch.nio.file.internal.Underlying;

import java.io.IOException;

/**
 */
public class PathImpl implements Path, Underlying<name.pachler.nio.file.Path>
{
    private final name.pachler.nio.file.Path impl;

    public PathImpl(final name.pachler.nio.file.Path impl)
    {
        this.impl = impl;
    }

    public static Path toPath(String fullPath)
    {
        return new PathImpl(name.pachler.nio.file.Paths.get(fullPath));
    }

    @Override
    public name.pachler.nio.file.Path underlying()
    {
        return impl;
    }

    @Override
    public WatchKey register(final WatchService watchService, final WatchEvent.Kind<?>... kinds) throws IOException
    {
        name.pachler.nio.file.WatchService ws = ToUnderlying.asWS(watchService);
        name.pachler.nio.file.WatchEvent.Kind<?> ks[] = ToUnderlying.asKinds(kinds);
        return wrap(impl.register(ws, ks));
    }

    @Override
    public WatchKey register(final WatchService watchService, final WatchEvent.Kind<?>[] kinds, final WatchEvent.Modifier... modifiers)
            throws IOException
    {
        name.pachler.nio.file.WatchService ws = ToUnderlying.asWS(watchService);
        name.pachler.nio.file.WatchEvent.Kind<?> ks[] = ToUnderlying.asKinds(kinds);
        name.pachler.nio.file.WatchEvent.Modifier mods[] = ToUnderlying.asModifiers(modifiers);
        return wrap(impl.register(ws, ks, mods));
    }

    @Override
    public Path resolve(final Path path)
    {
        PathImpl pathImpl = (PathImpl) path;
        return new PathImpl(impl.resolve(pathImpl.underlying()));
    }

    @Override
    public boolean equals(final Object obj)
    {
        return obj instanceof Underlying && impl.equals(((Underlying) obj).underlying());
    }

    @Override
    public int hashCode()
    {
        return impl.hashCode();
    }

    @Override
    public String toString()
    {
        return impl.toString();
    }

    private WatchKey wrap(final name.pachler.nio.file.WatchKey impl)
    {
        return impl == null ?  null : new WatchKeyImpl(impl);
    }

}
