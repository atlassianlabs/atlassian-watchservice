package com.atlassian.watch.nio.file.api;

/**
 *
 * This set of APIs are simple wrappers for two libraries.  The first is the Java 7 java.nio.file library that
 * adds file watching and the other is the excellent jWatch library that adds the same but on earlier Java versions.
 *
 * This library apes the official JDK API (as does jWatch) to minimise the cognitive dissonance in using it.
 *
 * You can read the official Java API documentation and then use this library to get cross Java version file watching.
 *
 * This library also fills some gaps even on Java 7.  For example at the time of writing the Mac implementation of the {@link WatchService}
 * is in fact a polling implementation that does not use native file watch libraries under the covers.  As such it is terrible slow.
 *
 * If this is detected then it will use the jWatch instead.
 *
 * See http://docs.oracle.com/javase/7/docs/api/java/nio/file/FileSystems.html
 */
