package com.atlassian.watch.nio.file.internal;

/**
 * Our little interface of underlying-ness
 */
public interface Underlying<T>
{
    T underlying();
}
