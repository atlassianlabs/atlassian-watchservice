package com.atlassian.watch.nio.file.api;

/**
 * 1-1 interface for WatchEvent
 *
 * See http://docs.oracle.com/javase/7/docs/api/java/nio/file/WatchEvent.html
 */
public interface WatchEvent<T>
{
    public T context();

    public int count();

    public WatchEvent.Kind<T> kind();

    public static interface Modifier  {
        java.lang.String name();
    }

    public static interface Kind <T>  {
        java.lang.String name();

        java.lang.Class<T> type();
    }

}
