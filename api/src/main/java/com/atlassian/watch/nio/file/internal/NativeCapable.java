package com.atlassian.watch.nio.file.internal;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.WatchService;

/**
 * This detects the Java version AND whether the underlying WatchService is truly capable of performant file watching.
 *
 * The PollingWatchService on Mac for example only REALLY checks for updates every 10 seconds.  A life time in other
 * words.  So we test for that and switch in the non native native jWatch in some cases
 */
public class NativeCapable
{
    private static boolean capable = tryForDecentWatcher();

    private static boolean tryForDecentWatcher()
    {
        boolean capable = false;
        double jdkVersion = getJavaVersion();
        if (jdkVersion >= 1.7)
        {
            //
            // ok try and load a WatcherService that is NOT a polling one.  On Mac Java 1.7.0_45 at least
            // it still had a polling watcher, which is truly abysmal
            //
            capable = true;
            WatchService watchService = null;
            try
            {
                watchService = FileSystems.getDefault().newWatchService();
                if (watchService.getClass().getSimpleName().contains("PollingWatchService"))
                {
                    capable = false;
                }
            }
            catch (IOException e)
            {
                //ignore
            }
            finally
            {
                closeQuietly(watchService);
            }
        }
        return capable;
    }

    private static double getJavaVersion()
    {
        String version = System.getProperty("java.version");
        int pos = 0, count = 0;
        for (; pos < version.length() && count < 2; pos++)
        {
            if (version.charAt(pos) == '.')
            {
                count++;
            }
        }
        pos--;
        return Double.parseDouble(version.substring(0, pos));
    }

    private static void closeQuietly(final WatchService watchService)
    {
        if (watchService != null)
        {
            try
            {
                watchService.close();
            }
            catch (IOException ignored)
            {
            }
        }
    }

    public static boolean isCapable()
    {
        return capable;
    }

    /*
     * Only use testing.
     */
    public static void set4Testing(boolean flag)
    {
        capable = flag;
    }
}
