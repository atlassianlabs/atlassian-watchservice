package com.atlassian.watch.nio.file.internal.java7;

import com.atlassian.watch.nio.file.api.WatchEvent;
import com.atlassian.watch.nio.file.api.WatchService;
import com.atlassian.watch.nio.file.internal.Underlying;

import java.util.ArrayList;
import java.util.List;

/**
 */
@SuppressWarnings ("unchecked")
public class ToUnderlying
{
    public static <P> P underlying(final Object obj)
    {
        Underlying<P> underlying = (Underlying<P>) obj;
        return underlying.underlying();
    }

    public static <P> P as(WatchService wrapper)
    {
        return underlying(wrapper);
    }

    public static java.nio.file.WatchEvent.Kind<?>[] asKinds(final WatchEvent.Kind<?>[] kinds)
    {
        List<java.nio.file.WatchEvent.Kind> mods = new ArrayList<java.nio.file.WatchEvent.Kind>(kinds.length);
        for (final WatchEvent.Kind k : kinds)
        {
            mods.add(WatchEventImpl.KindImpl.map(k));
        }
        return mods.toArray(new java.nio.file.WatchEvent.Kind[mods.size()]);
    }

    public static java.nio.file.WatchEvent.Modifier[] asModifiers(final WatchEvent.Modifier[] modifiers)
    {
        List<java.nio.file.WatchEvent.Modifier> mods = new ArrayList<java.nio.file.WatchEvent.Modifier>(modifiers.length);
        for (final WatchEvent.Modifier m : modifiers)
        {
            mods.add(WatchEventImpl.ModifierImpl.map(m));
        }
        return mods.toArray(new java.nio.file.WatchEvent.Modifier[mods.size()]);
    }
}
