package com.atlassian.watch.nio.file.internal.java7;

import com.atlassian.watch.nio.file.api.Path;
import com.atlassian.watch.nio.file.api.WatchEvent;
import com.atlassian.watch.nio.file.api.WatchKey;
import com.atlassian.watch.nio.file.api.WatchService;
import com.atlassian.watch.nio.file.internal.Underlying;

import java.io.IOException;
import java.nio.file.Paths;

/**
 */
public class PathImpl implements Path, Underlying<java.nio.file.Path>
{
    private final java.nio.file.Path impl;

    public PathImpl(final java.nio.file.Path impl)
    {
        this.impl = impl;
    }

    public static Path toPath(String fullPath)
    {
        return new PathImpl(Paths.get(fullPath));
    }

    @Override
    public java.nio.file.Path underlying()
    {
        return impl;
    }

    @Override
    public WatchKey register(final WatchService watchService, final WatchEvent.Kind<?>... kinds) throws IOException
    {
        java.nio.file.WatchService ws = ToUnderlying.as(watchService);
        java.nio.file.WatchEvent.Kind<?> ks[] = ToUnderlying.asKinds(kinds);
        return wrap(impl.register(ws, ks));
    }

    @Override
    public WatchKey register(final WatchService watchService, final WatchEvent.Kind<?>[] kinds, final WatchEvent.Modifier... modifiers)
            throws IOException
    {
        java.nio.file.WatchService ws = ToUnderlying.as(watchService);
        java.nio.file.WatchEvent.Kind<?> ks[] = ToUnderlying.asKinds(kinds);
        java.nio.file.WatchEvent.Modifier mods[] = ToUnderlying.asModifiers(modifiers);
        return wrap(impl.register(ws, ks, mods));
    }

    @Override
    public Path resolve(final Path path)
    {
        PathImpl pathImpl = (PathImpl) path;
        return new PathImpl(impl.resolve(pathImpl.underlying()));
    }

    @Override
    public boolean equals(final Object obj)
    {
        return obj instanceof Underlying && impl.equals(((Underlying) obj).underlying());
    }

    @Override
    public int hashCode()
    {
        return impl.hashCode();
    }

    @Override
    public String toString()
    {
        return impl.toString();
    }

    private WatchKey wrap(final java.nio.file.WatchKey impl)
    {
        return impl == null ? null : new WatchKeyImpl(impl);
    }
}
