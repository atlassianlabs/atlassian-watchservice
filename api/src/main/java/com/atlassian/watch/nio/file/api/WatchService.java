package com.atlassian.watch.nio.file.api;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 1-1 representation of the WatchService
 *
 * See http://docs.oracle.com/javase/7/docs/api/java/nio/file/WatchService.html
 */
public interface WatchService
{

    WatchKey poll() throws InterruptedException;

    WatchKey poll(long timeout, TimeUnit unit) throws InterruptedException;

    WatchKey take() throws InterruptedException;

    void close() throws IOException;
}
