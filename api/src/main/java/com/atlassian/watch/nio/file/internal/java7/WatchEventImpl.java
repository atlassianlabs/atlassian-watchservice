package com.atlassian.watch.nio.file.internal.java7;

import com.atlassian.watch.nio.file.api.Paths;
import com.atlassian.watch.nio.file.api.StandardWatchEventKinds;
import com.atlassian.watch.nio.file.api.WatchEvent;
import com.atlassian.watch.nio.file.internal.Underlying;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Document this class / interface here
 *
 * @since v0.x
 */
public class WatchEventImpl<T> implements WatchEvent<T>, Underlying<java.nio.file.WatchEvent>
{
    private final java.nio.file.WatchEvent<T> impl;

    public WatchEventImpl(final java.nio.file.WatchEvent<T> impl)
    {
        this.impl = impl;
    }

    @Override
    public java.nio.file.WatchEvent underlying()
    {
        return impl;
    }

    @SuppressWarnings ("unchecked")
    @Override
    public T context()
    {
        Path path = (Path) impl.context();
        return path == null ? null : (T) Paths.toPath(path.toString());
    }

    @Override
    public int count()
    {
        return impl.count();
    }

    @Override
    public Kind<T> kind()
    {
        return KindImpl.unmap(impl.kind());
    }

    @Override
    public boolean equals(final Object obj)
    {
        return obj instanceof Underlying && impl.equals(((Underlying) obj).underlying());
    }

    @Override
    public int hashCode()
    {
        return impl.hashCode();
    }

    @Override
    public String toString()
    {
        return impl.toString();
    }


    public static class KindImpl implements Kind, Underlying<java.nio.file.WatchEvent.Kind>
    {

        private static Map<Kind, java.nio.file.WatchEvent.Kind> map;

        static
        {
            map = new HashMap<Kind, java.nio.file.WatchEvent.Kind>();
            map.put(StandardWatchEventKinds.ENTRY_CREATE, java.nio.file.StandardWatchEventKinds.ENTRY_CREATE);
            map.put(StandardWatchEventKinds.ENTRY_DELETE, java.nio.file.StandardWatchEventKinds.ENTRY_DELETE);
            map.put(StandardWatchEventKinds.ENTRY_MODIFY, java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY);
            map.put(StandardWatchEventKinds.OVERFLOW, java.nio.file.StandardWatchEventKinds.OVERFLOW);
        }

        private final java.nio.file.WatchEvent.Kind impl;

        KindImpl(final java.nio.file.WatchEvent.Kind impl)
        {
            this.impl = impl;
        }

        static java.nio.file.WatchEvent.Kind map(Kind kind)
        {
            java.nio.file.WatchEvent.Kind theKind = map.get(kind);
            if (theKind == null)
            {
                throw new IllegalStateException("Could find kind " + kind.name());
            }
            return theKind;
        }

        @SuppressWarnings ("unchecked")
        public static <T> Kind<T> unmap(java.nio.file.WatchEvent.Kind kind)
        {
            for (Map.Entry<Kind, java.nio.file.WatchEvent.Kind> entry : map.entrySet())
            {
                if (entry.getValue() == kind)
                {
                    return entry.getKey();
                }
            }
            throw new IllegalStateException("Could find kind " + kind.name());
        }

        @Override
        public java.nio.file.WatchEvent.Kind underlying()
        {
            return impl;
        }

        @Override
        public String name()
        {
            return impl.name();
        }

        @Override
        public Class type()
        {
            return impl.type();
        }
    }

    public static class ModifierImpl implements Modifier, Underlying<java.nio.file.WatchEvent.Modifier>
    {
        private final java.nio.file.WatchEvent.Modifier impl;

        public ModifierImpl(final java.nio.file.WatchEvent.Modifier impl)
        {
            this.impl = impl;
        }

        @SuppressWarnings ("UnusedParameters")
        static java.nio.file.WatchEvent.Modifier map(Modifier modifier)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public java.nio.file.WatchEvent.Modifier underlying()
        {
            return impl;
        }

        @Override
        public String name()
        {
            return impl.name();
        }
    }
}