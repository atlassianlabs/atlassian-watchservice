package com.atlassian.watch.nio.file.api;


/**
 * See http://docs.oracle.com/javase/7/docs/api/java/nio/file/StandardWatchEventKinds.html
 */
public class StandardWatchEventKinds implements WatchEvent.Kind<Path>
{
    public static final WatchEvent.Kind<Path> ENTRY_CREATE = new StandardWatchEventKinds("create");
    public static final WatchEvent.Kind<Path> ENTRY_DELETE = new StandardWatchEventKinds("delete");
    public static final WatchEvent.Kind<Path> ENTRY_MODIFY = new StandardWatchEventKinds("modify");
    public static final WatchEvent.Kind<java.lang.Void> OVERFLOW = new WatchEvent.Kind<Void>()
    {
        @Override
        public String name()
        {
            return "overflow";
        }

        @Override
        public Class<Void> type()
        {
            return Void.class;
        }
    };

    private final String name;

    private StandardWatchEventKinds(final String name)
    {
        this.name = name;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public Class<Path> type()
    {
        return Path.class;
    }
}