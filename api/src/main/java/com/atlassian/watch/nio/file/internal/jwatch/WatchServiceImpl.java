package com.atlassian.watch.nio.file.internal.jwatch;

import com.atlassian.watch.nio.file.api.WatchKey;
import com.atlassian.watch.nio.file.api.WatchService;
import com.atlassian.watch.nio.file.internal.Underlying;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 */
public class WatchServiceImpl implements WatchService, Underlying<name.pachler.nio.file.WatchService>
{
    private final name.pachler.nio.file.WatchService impl;

    public WatchServiceImpl(final name.pachler.nio.file.WatchService impl)
    {
        this.impl = impl;
    }

    @Override
    public name.pachler.nio.file.WatchService underlying()
    {
        return impl;
    }

    private WatchKey wrap(final name.pachler.nio.file.WatchKey watchKey)
    {
        return watchKey != null ? new WatchKeyImpl(watchKey) : null;
    }

    @Override
    public WatchKey poll() throws InterruptedException
    {
        return wrap(impl.poll());
    }

    @Override
    public WatchKey poll(final long timeout, final TimeUnit unit) throws InterruptedException
    {
        return wrap(impl.poll(timeout, unit));
    }

    @Override
    public WatchKey take() throws InterruptedException
    {
        return wrap(impl.take());
    }

    @Override
    public void close() throws IOException
    {
        impl.close();
    }

    @Override
    public boolean equals(final Object obj)
    {
        return obj instanceof Underlying && impl.equals(((Underlying) obj).underlying());
    }

    @Override
    public int hashCode()
    {
        return impl.hashCode();
    }

    @Override
    public String toString()
    {
        return impl.toString();
    }


}
