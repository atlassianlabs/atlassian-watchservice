package com.atlassian.watch.nio.file.internal.jwatch;

import com.atlassian.watch.nio.file.api.Paths;
import com.atlassian.watch.nio.file.api.StandardWatchEventKinds;
import com.atlassian.watch.nio.file.api.WatchEvent;
import com.atlassian.watch.nio.file.internal.Underlying;
import name.pachler.nio.file.impl.PathWatchEventModifier;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Document this class / interface here
 *
 * @since v0.x
 */
@SuppressWarnings ("unchecked")
public class WatchEventImpl<T> implements WatchEvent<T>, Underlying<name.pachler.nio.file.WatchEvent>
{
    private final name.pachler.nio.file.WatchEvent<T> impl;

    public WatchEventImpl(final name.pachler.nio.file.WatchEvent<T> impl)
    {
        this.impl = impl;
    }

    @Override
    public name.pachler.nio.file.WatchEvent underlying()
    {
        return impl;
    }

    @Override
    public T context()
    {
        Object path = impl.context();
        return path == null ? null : (T) Paths.toPath(path.toString());
    }

    @Override
    public int count()
    {
        return impl.count();
    }

    @Override
    public Kind<T> kind()
    {
        return KindImpl.unmap(impl.kind());
    }

    @Override
    public boolean equals(final Object obj)
    {
        return obj instanceof Underlying && impl.equals(((Underlying) obj).underlying());
    }

    @Override
    public int hashCode()
    {
        return impl.hashCode();
    }

    @Override
    public String toString()
    {
        return impl.toString();
    }

    public static class KindImpl implements Kind, Underlying<name.pachler.nio.file.WatchEvent.Kind>
    {
        private static Map<Kind, name.pachler.nio.file.WatchEvent.Kind> map;

        static
        {
            map = new HashMap<Kind, name.pachler.nio.file.WatchEvent.Kind>();
            map.put(StandardWatchEventKinds.ENTRY_CREATE, name.pachler.nio.file.StandardWatchEventKind.ENTRY_CREATE);
            map.put(StandardWatchEventKinds.ENTRY_DELETE, name.pachler.nio.file.StandardWatchEventKind.ENTRY_DELETE);
            map.put(StandardWatchEventKinds.ENTRY_MODIFY, name.pachler.nio.file.StandardWatchEventKind.ENTRY_MODIFY);
            map.put(StandardWatchEventKinds.OVERFLOW, name.pachler.nio.file.StandardWatchEventKind.OVERFLOW);
        }

        private final name.pachler.nio.file.WatchEvent.Kind impl;

        KindImpl(final name.pachler.nio.file.WatchEvent.Kind kind)
        {
            this.impl = kind;
        }

        static name.pachler.nio.file.WatchEvent.Kind map(Kind kind)
        {
            name.pachler.nio.file.WatchEvent.Kind theKind = map.get(kind);
            if (theKind == null)
            {
                throw new IllegalStateException("Could find kind " + kind.name());
            }
            return theKind;
        }

        public static <T> Kind<T> unmap(name.pachler.nio.file.WatchEvent.Kind kind)
        {
            for (Map.Entry<Kind, name.pachler.nio.file.WatchEvent.Kind> entry : map.entrySet())
            {
                if (entry.getValue() == kind)
                {
                    return entry.getKey();
                }
            }
            throw new IllegalStateException("Could find kind " + kind.name());
        }

        @Override
        public name.pachler.nio.file.WatchEvent.Kind underlying()
        {
            return impl;
        }

        @Override
        public String name()
        {
            return impl.name();
        }

        @Override
        public Class type()
        {
            return impl.type();
        }
    }

    public static class ModifierImpl implements Modifier, Underlying<name.pachler.nio.file.WatchEvent.Modifier>
    {
        private final name.pachler.nio.file.WatchEvent.Modifier impl;

        public ModifierImpl(final Modifier modifier)
        {
            this.impl = new PathWatchEventModifier(modifier.name());
        }

        static name.pachler.nio.file.WatchEvent.Modifier map(Modifier modifier)
        {
            return new PathWatchEventModifier(modifier.name());
        }

        @Override
        public name.pachler.nio.file.WatchEvent.Modifier underlying()
        {
            return impl;
        }

        @Override
        public String name()
        {
            return impl.name();
        }
    }
}