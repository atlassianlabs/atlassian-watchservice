package com.atlassian.watch.nio.file.api;

/**
 * 1-1 interface for Path
 *
 * See http://docs.oracle.com/javase/7/docs/api/java/nio/file/Path.html
 */
public interface Path
{
    public WatchKey register(WatchService watchService, WatchEvent.Kind<?>... kinds) throws java.io.IOException;

    public WatchKey register(WatchService watchService, WatchEvent.Kind<?>[] kinds, WatchEvent.Modifier... modifiers) throws java.io.IOException;

    public Path resolve(Path path);
}
