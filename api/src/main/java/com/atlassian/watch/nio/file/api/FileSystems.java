package com.atlassian.watch.nio.file.api;

import com.atlassian.watch.nio.file.internal.NativeCapable;

import java.io.IOException;

/**
 * See http://docs.oracle.com/javase/7/docs/api/java/nio/file/FileSystems.html
 */
public class FileSystems
{
    public static FileSystem getDefault()
    {
        return new FileSystem();
    }

    public static class FileSystem
    {
        private FileSystem()
        {
        }

        public WatchService newWatchService() throws IOException
        {
            if (NativeCapable.isCapable())
            {
                return jdk7WatchService();
            }
            else
            {
                return jWatchService();
            }
        }

        private WatchService jdk7WatchService() throws IOException
        {
            return new com.atlassian.watch.nio.file.internal.java7.WatchServiceImpl(java.nio.file.FileSystems.getDefault().newWatchService());
        }

        private WatchService jWatchService()
        {
            return new com.atlassian.watch.nio.file.internal.jwatch.WatchServiceImpl(name.pachler.nio.file.FileSystems.getDefault().newWatchService());
        }
    }
}
