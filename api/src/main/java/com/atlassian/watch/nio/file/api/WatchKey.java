package com.atlassian.watch.nio.file.api;

import java.util.List;

/**
 * 1-1 representation of the WatchKey
 *
 * See http://docs.oracle.com/javase/7/docs/api/java/nio/file/WatchKey.html
 */
public interface WatchKey
{
    void cancel();

    boolean isValid();

    <T> List<WatchEvent<T>> pollEvents();

    boolean reset();
}
