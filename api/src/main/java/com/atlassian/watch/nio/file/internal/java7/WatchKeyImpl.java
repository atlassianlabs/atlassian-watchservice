package com.atlassian.watch.nio.file.internal.java7;

import com.atlassian.watch.nio.file.api.WatchEvent;
import com.atlassian.watch.nio.file.api.WatchKey;
import com.atlassian.watch.nio.file.internal.Underlying;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class WatchKeyImpl implements WatchKey, Underlying<java.nio.file.WatchKey>
{
    private final java.nio.file.WatchKey impl;

    public WatchKeyImpl(final java.nio.file.WatchKey impl)
    {
        this.impl = impl;
    }

    @Override
    public java.nio.file.WatchKey underlying()
    {
        return impl;
    }

    @Override
    public void cancel()
    {
        impl.cancel();
    }

    @Override
    public boolean isValid()
    {
        return impl.isValid();
    }

    @Override
    public boolean reset()
    {
        return impl.reset();
    }

    @Override
    @SuppressWarnings ("unchecked")
    public List<WatchEvent<?>> pollEvents()
    {
        return wrap(impl.pollEvents());
    }

    @Override
    public boolean equals(final Object obj)
    {
        return obj instanceof Underlying && impl.equals(((Underlying) obj).underlying());
    }

    @Override
    public int hashCode()
    {
        return impl.hashCode();
    }

    @Override
    public String toString()
    {
        return impl.toString();
    }

    private List<WatchEvent<?>> wrap(final List<java.nio.file.WatchEvent<?>> underlying)
    {
        if (underlying == null)
        {
            return null;
        }

        List<WatchEvent<?>> watchEvents = new ArrayList<WatchEvent<?>>(underlying.size());
        for (java.nio.file.WatchEvent<?> watchEvent : underlying)
        {
            //noinspection unchecked
            watchEvents.add(new WatchEventImpl(watchEvent));
        }
        return watchEvents;
    }
}
