package com.atlassian.watch.nio.file.api;

import com.atlassian.watch.nio.file.internal.NativeCapable;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * These tests are HIGHLY time dependant.  The worse kind of test to have.  But file watching is an inherently asynch
 * operation that may take a long time.
 * <p/>
 * So is it better to have not test or some test or....
 * <p/>
 * So I have escape hatches in the form of BOOLEAN statics....I am so sorry!
 */
@SuppressWarnings ("ResultOfMethodCallIgnored")
public class TestWrapping
{
    public static final String NEW_FILE = "newFile";
    private static final boolean I_WANT_TO_JUST_MVN_RELEASE_THIS = false;
    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private File dirUnderTest;

    @SuppressWarnings ("ResultOfMethodCallIgnored")
    @Before
    public void setUp() throws Exception
    {
        File someFile = File.createTempFile("someDir", "");
        someFile.delete();
        someFile.mkdirs();
        someFile.deleteOnExit();

        dirUnderTest = someFile;
    }

    @Test
    public void testViaDetection() throws Exception
    {
        if (I_WANT_TO_JUST_MVN_RELEASE_THIS)
        {
            return;
        }

        standardBatteryOfTests(FileSystems.getDefault().newWatchService());

        pollWatchKeyBatteryOfTests(FileSystems.getDefault().newWatchService());
    }

    @Test
    public void testJava7Libs() throws Exception
    {
        if (I_WANT_TO_JUST_MVN_RELEASE_THIS)
        {
            return;
        }

        /** make it use JDK 7 explicitly */
        NativeCapable.set4Testing(true);

        standardBatteryOfTests(FileSystems.getDefault().newWatchService());

        pollWatchKeyBatteryOfTests(FileSystems.getDefault().newWatchService());

    }

    @Test
    public void testJWatchLibs() throws Exception
    {
        if (I_WANT_TO_JUST_MVN_RELEASE_THIS)
        {
            return;
        }

        /** make it use jWatch explicitly */
        NativeCapable.set4Testing(false);

        standardBatteryOfTests(FileSystems.getDefault().newWatchService());

        pollWatchKeyBatteryOfTests(FileSystems.getDefault().newWatchService());
    }


    /**
     * These tests use a standard approach of take() being called in the watchService itself as opposed to polling
     *
     * @param watchService the watch service to test
     * @throws IOException just cause
     * @throws InterruptedException also just cause
     */
    private void standardBatteryOfTests(final WatchService watchService) throws IOException, InterruptedException
    {
        sout("Starting standard battery of tests....");

        long then = System.currentTimeMillis();

        /*
         * Both JWatch and Java have equal semantics on keys.  The key you register with its equal to the key you get back
         */
        Map<WatchKey, Path> keyPathMap = new HashMap<WatchKey, Path>();

        CountDownLatch createLatch = new CountDownLatch(1);
        CountDownLatch deleteLatch = new CountDownLatch(1);

        Path path = Paths.toPath(dirUnderTest);
        File newFile = new File(dirUnderTest, NEW_FILE);
        executor.submit(createFile(createLatch, newFile));

        WatchKey registerKey = path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
        keyPathMap.put(registerKey, path);

        createLatch.countDown();

        Callable<WatchKey> watchServicePoll = new Callable<WatchKey>()
        {
            @Override
            public WatchKey call() throws Exception
            {
                sout("watchService.poll(500)");
                return watchService.poll(500, TimeUnit.MILLISECONDS);
            }
        };
        WatchKey createKey = within(30, watchServicePoll);



        assertNewFile(createKey.<Path>pollEvents(), NEW_FILE);

        // cause it to be ready to polled
        createKey.reset();

        // now get ready to delete the file
        executor.submit(deleteFile(deleteLatch, newFile));

        deleteLatch.countDown();

        sout("watchService.take()");
        WatchKey deleteKey = watchService.take();

        //
        // assert set semantics
        assertThat(deleteKey.equals(createKey),equalTo(true));
        assertThat(keyPathMap.containsKey(deleteKey), equalTo(true));

        assertDeleteFile(deleteKey.<Path>pollEvents(), NEW_FILE);

        sout(format("This test took %d ms to run", System.currentTimeMillis() - then));
    }

    /**
     * These tests take a watch service and use a poll approach to getting results as opposed to a take() approach
     *
     * @param watchService the watch service to test
     * @throws IOException just cause
     * @throws InterruptedException and also just cause
     */
    private void pollWatchKeyBatteryOfTests(final WatchService watchService) throws IOException, InterruptedException
    {
        /**
         * Testing has shown that the jWatch code cant handle the eventKey.poll() approach to getting
         * events. It must use watchService.take().
         *
         * So we skip this test when we are not using the native API.
         */
        sout("Starting watchKey polling tests....");
        if (!NativeCapable.isCapable())
        {
            sout("Not capable.  Skipping....");
            return;
        }
        long then = System.currentTimeMillis();

        CountDownLatch createLatch = new CountDownLatch(1);
        CountDownLatch deleteLatch = new CountDownLatch(1);

        Path path = Paths.toPath(dirUnderTest);
        File newFile = new File(dirUnderTest, NEW_FILE);
        executor.submit(createFile(createLatch, newFile));

        final WatchKey watchKey = path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
        createLatch.countDown();

        List<WatchEvent<Path>> watchEvents = listWithin(30, pollCallableOnKey(watchKey));

        assertNewFile(watchEvents, NEW_FILE);

        // now get ready to delete the file
        executor.submit(deleteFile(deleteLatch, newFile));

        deleteLatch.countDown();

        watchEvents = listWithin(30, pollCallableOnKey(watchKey));

        assertDeleteFile(watchEvents, NEW_FILE);

        watchKey.cancel();
        assertThat(watchKey.isValid(), equalTo(false));

        sout(format("This test took %d ms to run", System.currentTimeMillis() - then));
    }

    private void assertNewFile(List<WatchEvent<Path>> watchEvents, String fileName)
    {
        assertThat(watchEvents.size(), CoreMatchers.is(1));

        WatchEvent<Path> watchEvent = watchEvents.get(0);
        String newFileName = watchEvent.context().toString();

        assertThat(watchEvent.context(), CoreMatchers.instanceOf(Path.class));
        assertThat(watchEvent.count(), equalTo(1));
        assertThat(watchEvent.kind(), equalTo(StandardWatchEventKinds.ENTRY_CREATE));
        assertThat(watchEvent.kind().type(), equalTo(Path.class));
        assertThat(newFileName, equalTo(fileName));
    }

    private void assertDeleteFile(final List<WatchEvent<Path>> watchEvents, String fileName)
    {
        final WatchEvent<Path> watchEvent;
        final String newFileName;

        assertThat(watchEvents.size(), CoreMatchers.is(1));
        watchEvent = watchEvents.get(0);
        newFileName = watchEvent.context().toString();

        assertThat(watchEvent.kind(), equalTo(StandardWatchEventKinds.ENTRY_DELETE));
        assertThat(newFileName, equalTo(fileName));
    }

    private Callable<List<WatchEvent<Path>>> pollCallableOnKey(final WatchKey watchKey)
    {
        return new Callable<List<WatchEvent<Path>>>()
        {
            @Override
            public List<WatchEvent<Path>> call() throws Exception
            {
                List<WatchEvent<Path>> events = watchKey.pollEvents();
                boolean reset = watchKey.reset();
                if (!reset)
                {
                    throw new IllegalStateException("Didn't expect the watchKey reset to fail");
                }
                return events;
            }
        };
    }

    private <T> T within(final int secs, final Callable<T> callable)
    {
        List<T> list = listWithin(secs, new Callable<List<T>>()
        {
            @Override
            public List<T> call() throws Exception
            {
                T call = callable.call();
                if (call != null)
                {
                    List<T> l = new ArrayList<T>();
                    l.add(call);
                    return l;
                }
                return Collections.emptyList();
            }
        });
        return list.get(0);
    }

    private <T> List<T> listWithin(final int secs, final Callable<List<T>> callable)
    {
        long then = System.currentTimeMillis();
        long last = 0;
        while (true)
        {
            try
            {
                long now = System.currentTimeMillis();
                if ((now - then) > (secs * 1000))
                {
                    return Collections.emptyList();
                }
                if ((now - last) > (1000))
                {
                    sout("waiting....");
                    last = now;
                }
                List<T> ret = callable.call();
                if (ret != null && !ret.isEmpty())
                {
                    return ret;
                }
                sleep(10);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    private void sleep(final int i)
    {
        try
        {
            Thread.sleep(i);
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
    }

    private Runnable deleteFile(final CountDownLatch deleteLatch, final File file)
    {
        return new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    deleteLatch.await();
                }
                catch (InterruptedException e)
                {
                    throw new RuntimeException(e);
                }
                sout(format("Deleting file %s ", file.getAbsolutePath()));
                file.delete();
            }
        };
    }

    private Runnable createFile(final CountDownLatch latch, final File newFile)
    {
        return new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    latch.await();
                }
                catch (InterruptedException e)
                {
                    throw new RuntimeException(e);
                }
                try
                {
                    sout(format("Creating file %s ", newFile.getAbsolutePath()));
                    newFile.createNewFile();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        };
    }

    private void sout(String msg)
    {
        System.out.println(new SimpleDateFormat("hh:mm:ss").format(new Date()) + " - " + msg);
    }
}
