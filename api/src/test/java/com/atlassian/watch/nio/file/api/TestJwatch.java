package com.atlassian.watch.nio.file.api;

import com.atlassian.watch.nio.file.internal.NativeCapable;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TestJwatch {

    private static final int FILE_COUNT = 5500;

    @Test
    public void checkIfJPathWatchWorksWhenOver1024FileDescriptorsAreOpen() throws Exception {

        if (isMacOS()) {
            /** make it use jWatch explicitly */
            NativeCapable.set4Testing(false);
        }

        File rootTempDir = createTempDir("a-lot-of-dirs-inside-");

        List<File> aLotOfFilesList = new ArrayList<File>();
        for (int i = 0; i < FILE_COUNT; i++) {
            aLotOfFilesList.add(File.createTempFile("one-of-many-", ".tmp", rootTempDir));
        }

        List<FileInputStream> fileInputStreamList = new ArrayList<FileInputStream>();
        try {
            for (File file : aLotOfFilesList) {
                fileInputStreamList.add(new FileInputStream(file));
            }
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.toPath(rootTempDir);
            path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
            watchService.poll();
        } finally {
            for (FileInputStream fis : fileInputStreamList) {
                fis.close();
            }
        }

    }

    private File createTempDir(String name) throws IOException {
        return createTempDir(name, null);
    }

    private File createTempDir(String name, File parentDir) throws IOException {
        File tempDir = parentDir == null ?
                File.createTempFile(name, UUID.randomUUID().toString()) :
                File.createTempFile(name, UUID.randomUUID().toString(), parentDir);

        if (tempDir.delete() && tempDir.mkdir()) {
            return tempDir;
        } else {
            throw new IOException("Could not create temporary directory");
        }
    }

    public boolean isMacOS() {
        return System.getProperty("os.name", "").toLowerCase().contains("mac");
    }
}
